class CreateDateWiseNavs < ActiveRecord::Migration[5.2]
  def change
    create_table :date_wise_navs do |t|
      t.string :scheme_name
      t.decimal :nav,precision: 10, scale: 3
      t.date :nav_date
      t.timestamps
    end
  end
end
