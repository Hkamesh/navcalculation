Rails.application.routes.draw do
  root to: 'nav_calculation#index'
  resources :nav_calculation
end
