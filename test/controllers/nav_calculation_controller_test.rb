require 'test_helper'

class NavCalculationControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get nav_calculation_index_url
    assert_response :success
  end

  test "should get show" do
    get nav_calculation_show_url
    assert_response :success
  end

end
