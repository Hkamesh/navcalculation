require 'csv'
namespace :move_nav_data_to_db do
	task import_nav_data_in_csv: :environment do
		file_path = Rails.root.join('axis_fund_data.csv')
		csv = CSV.read(file_path.to_s)
		csv.shift
		csv.each do |row|
			scheme_name = row[1]
			date = Date.parse(row[7])
			nav = row[4]
			DateWiseNav.create(scheme_name: scheme_name,nav: nav.to_f,nav_date: date)
		end
	end
end
