include Finance

class NavCalculationController < ApplicationController
  def index
  end

  def show
  end

  def create
  	scheme_name = params[:scheme_name]
  	dates = params[:date]
  	amount = params[:amount]

  	data_schema = []
  	scheme_name.each_with_index do |value, index|
      data_schema << {scheme: value,investment_date: dates[index],amount_invested: amount[index]}
  	end

    scheme_unit_map = {}
    @scheme_current_value_map = {}

    trans = []

    data_schema.each do |data| 
      if(data[:amount_invested].present? && data[:investment_date].present?)
        nav_for_given_date       = DateWiseNav.find_by(scheme_name: data[:scheme],nav_date: Date.parse(data[:investment_date]))&.nav
        if nav_for_given_date.present?
          units_alloted_for_amount = data[:amount_invested].to_f/nav_for_given_date
          if scheme_unit_map.has_key? data[:scheme].to_sym
            new_units = scheme_unit_map[data[:scheme].to_sym] + units_alloted_for_amount
            scheme_unit_map[data[:scheme]] = new_units
          else
            scheme_unit_map[data[:scheme]] = units_alloted_for_amount
          end
          trans << Transaction.new( data[:amount_invested].to_i, date: Time.new(data[:investment_date]))
        end
      end
    end

    @xirr = trans.xirr.apr.to_f.round(2) rescue nil

    scheme_unit_map.each do |scheme,units|
      scheme_last_nav = DateWiseNav.where(scheme_name: scheme).order(nav_date: :desc).first.nav
      @scheme_current_value_map[scheme] = scheme_last_nav*scheme_unit_map[scheme]
    end

    respond_to do |format|
      format.js
      format.json {render json: @scheme_current_value_map}
    end
  end

end
