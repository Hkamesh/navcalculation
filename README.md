# README


* Ruby version
    2.6.2
* Mysql Version
    8.0.15
* Configuration
    
* Database creation
	```
	CREATE USER 'nav_calculation'@'localhost' IDENTIFIED BY 'nav_calculation';
	GRANT ALL PRIVILEGES ON nav_calculation.* TO 'nav_calculation'@'localhost';
	```

How to start server:

    `bin/rails db:migrate`
    `bin/rails move_nav_data_to_db:import_nav_data_in_csv`
    `rails s`

    visit localhost:3000/nav_calculation
    
